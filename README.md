Teste para fazer push no gitlab
* git init - inicia o versionamento no git

* git add - envia os arquivos selecionados para a área de standy, antes de guardar no repositorio.
Ex: git add README.md

* git status - verifica arquivos que estão na área de standy, aguardando para serem commitados.

* git commit - é o comando que envia os arquivo da área de Standy para o repositório. -m é o comando para enviar junto uma mensagem, que nocaso ficaria como o titulo desse commit executado.
Ex: git commit -m "Meu primeiro Commit"

* git branch -M - é o comando caso queira mudar o nome do Branch, ou linha do tempo, no caso a linha do tempo principal se chama master, mas o Git está migrando para Main.
Ex: git branch -M "main"

* ssh-keygen -t ed25519 -C <msg> - para adquirir chave keygen para conectar o Git e GitLab

* ssh -T git@gitlab.com  - para testar se a conexão foi efetuada e deve retornar a seguinte mensagem:
    "The authenticity of host 'gitlab.com       (****:****:**:*:****:****:****:****)' can't
     be established.
     ECDSA key fingerprint is
     AAA999:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
     Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
     Warning: Permanently added 'gitlab.com, ****:****:**:*:****:****:****:****' (ECDSA) to the list of known hosts.
     Welcome to GitLab, @Nome_de_Usuário"
Efetue novamente o comando que se tudo occorem bem retornará uma mensagem que de bem vindo!:
    "Welcome to GitLab, @Nome_de_Usuário!"

* git remote add origin <seu link de acesso fornecido na criação do repositorio GitLab> - pode ser por HTTPS ou chave keygen ssh ou rsa.
Ex: git@gitlab.com:<suaPasta/<nomeprojeto.git>> 

* git push -u origin all - esse comando detalha a inserção dos arquivos do repositorio na nuvem Gitlab

* git push -u origin -tags -  verifica se sua chaves estão corretas

* git add . - Depois de feita algumas alterações e inserção de novos arquivos, o comando para enviar para o repositorio é o "add", mas podemos usar o "add .", no qual enviará todos os arquivos de uma só vez. Usando o comando "git status" verificamos que os arquivos enviados para a area de standy nos é mostrado como:
modified: quando o arquivo já existe no repositório e foi modificado.
new filw: quando é um novo arquivo inserido no projeto.
E podemos de fato enviar os arquivos para o repositório usando novamente o comando "git commit -m "<mensagem que vai aparecer na linha do tempo>".
Aparecerá na tela uma mensagem de criação do commit.

* git push origin master -  enviará do repositorio Git para o Gitlab já configurado.

<------------criando uma Branch (linha do tempo)--------------->

* git checkout -b "<mensagem>" - Uma branch é uma linha do tempo onde vc pode fazer alterações sem correr o risco de danificar o projeto original. 
O codigo -b cria uma nova branch com o nome que estiver na Mensagem
O codigo checkout sai da branch que está, no caso a master e vai para o branch que está sendo criada.
Ex: git checkout -b "novo-botao"
No Git bash será mostrado na linha de codigo que em qual branch está setado, e no Vscode também mostrará no canto inferior esquerdo.

* git add . - Na nova Branch o procedimento será o mesmo, adiciona com o "add ." no standy no repositorio Git.

* git commit -m "Novo botão que muda de cor o texto" - Faz o Commit com as alterações para a nova branch.

* git push origin novo-botao - Envia os arquivos do repositorio Git para o Repositorio Gitlab criando também uma nova branch.

* git checkout <nome da branch> - Muda de branch.

<------------Juntando as Branch's (linhas do tempo)------------->

* git checkout master - Para retornar para a Branch principal

* git merge <nome da branch> -  Depois que estar setado na Branch principal, o comando "merge, fará a junção das duas branch's.
Ex: git merge novo-botao

* git push origin master -  Aqui faremos novamente o comando para depois de juntar as Branch's no repositorio Git também faze-las no repositorio GitLab.

<------------------Clonar repositorio do Gitlab------------------>

Entrar no Repositorio do qual deseja Clonar o projeto, copia o link do projeto, cria uma nova pasta no computador que irá receber o Clone do repositório, abre o Git Bash dentro da pasta criada.

* git clone <link.git> - Execute o comando dentro do Git Bash setado na pasta destino

* git pull - Estando setado no projeto, o codigo "pull" busca no Repositorio GitLab toda alteração que possa ter sido feita no projeto, alterando o Clone no Repositorio Git.